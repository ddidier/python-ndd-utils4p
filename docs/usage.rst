#####
Usage
#####


TODO



``ndd_utils4p.commons``
=======================

.. currentmodule:: ndd_utils4p.commons

.. autosummary::
   :nosignatures:

   Exporter
   FileSerializer



``ndd_utils4p.git``
===================

In order to use the Git utilities, you must install the extra dependencies ``feature_git``, i.e. ``pip install ndd-utils4p[feature_git]``.

.. currentmodule:: ndd_utils4p.git

.. autosummary::
   :nosignatures:

   GitRepository


``ndd_utils4p.jinja``
=====================

.. currentmodule:: ndd_utils4p.jinja

.. autosummary::
   :nosignatures:

   JinjaFileSerializer
   JinjaExporter



``ndd_utils4p.json``
====================

.. currentmodule:: ndd_utils4p.json

.. autosummary::
   :nosignatures:

   JsonSchemaValidator



``ndd_utils4p.text``
====================

.. currentmodule:: ndd_utils4p.text

.. autosummary::
   :nosignatures:

   FileSerializer



``ndd_utils4p.yaml``
====================

.. currentmodule:: ndd_utils4p.yaml

.. autosummary::
   :nosignatures:

   YamlExporter
   YamlFileSerializer
