
.. include:: ../README.rst


Contents
========

.. toctree::
   :maxdepth: 2

   Usage <usage>
   Reference <api/modules>
   Development <development>
   License <license>
   Authors <authors>
   Changelog <changelog>


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
