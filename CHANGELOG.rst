#########
Changelog
#########


Version 0.1.0
=============

- Add ``ndd_utils4p.commons.Exporter``
- Add ``ndd_utils4p.commons.FileSerializer``
- Add ``ndd_utils4p.git.GitRepository``
- Add ``ndd_utils4p.jinja.JinjaFileSerializer``
- Add ``ndd_utils4p.jinja.JinjaExporter``
- Add ``ndd_utils4p.json.JsonSchemaValidator``
- Add ``ndd_utils4p.text.FileSerializer``
- Add ``ndd_utils4p.yaml.YamlExporter``
- Add ``ndd_utils4p.yaml.YamlFileSerializer``
- Add Flake8 linter
- Add Pylint linter
- Add Sphinx documentation
- Add Tox testing
- Add testing and code coverage to GitLab CI
